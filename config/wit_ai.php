<?php

return [
    'version' => env('WIT_AI_VERSION', '20170307'),
    'app_id' => env('WIT_AI_APP_ID'),
    'access_token' => [
        'server' => env('WIT_AI_ACCESS_TOKEN_SERVER'),
        'client' => env('WIT_AI_ACCESS_TOKEN_CLIENT')
    ]
];