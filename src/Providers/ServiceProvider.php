<?php

namespace Compagnie\Wit\Providers;

/**
 * Class ServiceProvider
 * @package Compagnie\Wit\Providers
 * @author Arnor D'Haenens <arnor.dhaenens@mortierbrigade.com>
 */
class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     * @var bool
     */
    public $defer = true;

    /**
     * Register function
     */
    public function register()
    {
        // merge configuration file with user overrides
        $this->mergeConfigFrom(
            __DIR__ . '../../config/wit_ai.php', 'wit_ai'
        );
    }

    /**
     * Boot function.
     */
    public function boot()
    {
        // publish the configuration file to the configuration directory
        $this->publishes(
            [
                __DIR__ . '../../config/wit_ai.php' => config_path('wit_ai.php')
            ],
            'config'
        );
    }

    /**
     * @return array
     */
    public function provides(): array
    {
        return [];
    }
}