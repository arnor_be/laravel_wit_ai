<?php

namespace Compagnie\Wit;

/**
 * Class Client
 * @package Compagnie\Wit
 * @author Arnor D'Haenens <arnor@arnor.be>
 */
class Client
{
    const BASE_URI = 'https://api.wit.ai/';
    protected $guzzleClient;
}